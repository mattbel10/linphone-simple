/*
 * Copyright 2012-2016 Canonical Ltd.
 *
 * This file is part of dialer-app.
 *
 * dialer-app is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * dialer-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Qt.labs.settings 1.0
import QtQuick.LocalStorage 2.0

import ServiceControl 1.0
import Linphone 1.0

import "components"
import "js/db.js" as FavContactsDB

Page {
    id: mainPage
    //Component.onCompleted: linphone.run()
    anchors.fill: parent
    signal updateContactList

    property bool inLandscape: parent.width > parent.height

    property bool applicationActive: Qt.application.active
    property string ussdResponseTitle: ""
    property string ussdResponseText: ""
    property string statusTextReceived: ""
    property bool greeterMode: (state == "greeterMode")
    property bool telepathyReady: false
    property bool onCall: false
    property bool onCallFav: false //Pressing a Favorite Contact should enable callButton
    property bool incomingCall: false
    property bool answered: false
    property bool speakerEnabled: true
    property bool keypadVisible: false
    property var currentStack: mainView.greeterMode ? pageStackGreeterMode : pageStackNormalMode
    property var bottomEdge: null
    property int iconRotation

    property string showId: ""
    property string showDomain: ""

    implicitWidth: units.gu(40)
    implicitHeight: units.gu(71)

    property string pendingNumberToDial: ""
    property bool accountReady: false

    property var theming: Settings {
        property string buttonColor: UbuntuColors.green
    }

    header: MainHeader {
        id: header
        title: "Linphone"
        flickable: mainFlickable
    }

    Component.onCompleted: {
        //Check if opened the app because we have an incoming call
        if (args.values.url && args.values.url.match(/^linphone/)) {

            console.log("Incoming Call on Closed App")
            showIncomingCall(args.values.url);

        } else if (Qt.application.arguments && Qt.application.arguments.length > 0) {

            for (var i = 0; i < Qt.application.arguments.length; i++) {
                if (Qt.application.arguments[i].match(/^linphone/)) {
                    showIncomingCall(Qt.application.arguments[i]);
                }

            }

        }

        //Start timer for Registering Status
        checkStatus.start()

    }

    ActiveAccount {
        id: activeAccount

        anchors.top: header.bottom
        //anchors.topMargin: header.height
        account: i18n.tr("offline")
        z: 100
    }

    Flickable {
        id: mainFlickable
        anchors.fill: parent
        //anchors.bottom: whiteRect.top //should anchor on top of the TextField but it doesn't (?)
        //anchors.bottomMargin: sipCall.height + callButton.height + units.gu(6)

        contentHeight: mainCol.visible ? mainCol.height + whiteRect.height + units.gu(6) :  outgoingCallComponent.height + whiteRect.height + units.gu(6)

        OutgoingCall {
            id: outgoingCallComponent
            anchors.topMargin: inLandscape ? units.gu(6) : units.gu(10)
            visible: !mainCol.visible

            showId: ""
            showDomain: ""
        }

        Column {
            id: mainCol
            visible: !onCall
            width: parent.width - marginColumn * 2
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter

            //Check how flicks on top of ActiveAccount
            anchors.topMargin: activeAccount.height + units.gu(2)
            spacing: units.gu(2.5)

            FavoriteContacts {
                id: favoriteContacts
            }

            Divider {
            }

            ListOfContacts {
                width: parent.width + marginColumn * 2
            }

        }

    }

    ActionsRow {
        id: userActions
        visible: outgoingCallComponent.visible
        anchors.bottom: whiteRect.top
    }
    
    Rectangle {
        id: whiteRect
        anchors.bottom: parent.bottom
        width: parent.width
        height: sipCall.visible ? sipCall.height + callButton.height + units.gu(6) : callButton.height + units.gu(4)

        //Fix: Flickable anchors to bottom of the page. Then tap gets on the bottom recent contact
        MouseArea {
            id: preventClickUnder
            anchors.fill: parent
        }

        TextField {
            id: sipCall
            width: parent.width-units.gu(3)
            visible: mainCol.visible

            //Avoid Language aids to the enable return buttons properly
            //See: https://gitlab.com/ubports-linphone/linphone-simple/issues/16
            inputMethodHints: Qt.ImhNoPredictiveText

            placeholderText: i18n.tr("SIP address to call")
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: callButton.top
            anchors.bottomMargin: units.gu(2)
            onAccepted: callButton.clicked()
            //Dirty workaround as inputMethodHints seems to take only one option at a time //inputMethodHints: Qt.ImhUrlCharactersOnly
            onTextChanged: sipCall.text = sipCall.text.toLowerCase()
        }

        CallButton {
            id: callButton
            objectName: "callButton"
            enabled: sipCall.text!=="" || onCallFav ? true : false //mainView.telepathyReady
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: units.gu(2)

            iconRotation: onCall ? 225 : 0
            callColor: onCall ? UbuntuColors.red : defaultColor

            onClicked: {
                //mainCol.visible = !mainCol.visible

                //Fix: do a js function
                if (!onCall) {
                    console.log("Try to call to " + sipCall.text)
                    onCall = true
                    mainCol.visible = false

                    //If you try to call a «regular» number, add it the domain
                    if (sipCall.text.indexOf("@") == -1 && accountInfo.lastDomain !== "") { sipCall.text = sipCall.text + "@" + accountInfo.lastDomain}

                    //Set info in outgoingCall
                    outgoingCallComponent.showId = sipCall.text.split("@")[0]
                    outgoingCallComponent.showDomain = sipCall.text.split("@")[1]

                    addAddressToFavorite(sipCall.text)
                    updateContactList()

                    Linphone.disableSpeaker();
                    speakerEnabled = false;

                    // TODO check if the user provided a non-standard port
                    //Replace 'sip:' 'http(s):' '/' ':number'
                    Linphone.call("sip:" + sipCall.text.replace(/sip\:|\:(\d+)|https\:|http\:|\//gi,"") + ":5060")
                    sipCall.text = sipCall.text

                } else {
                    // !onCall
                    sipCall.text = ""
                    console.log("Hanging up")
                    Linphone.terminate()
                    Linphone.enableSpeaker();
                    speakerEnabled = true;
                    //onCallFav = false
                    //mainCol.visible = true
                }
            }
        }
    }

    Arguments {
        id: args

        Argument {
            name: 'url'
            help: i18n.tr('Incoming Call from URL')
            required: false
            valueNames: ['URL']
        }
    }

    Connections {
        target: UriHandler

        onOpened: {
            console.log('Open from UriHandler')

            if (uris.length > 0) {
                console.log('Incoming call from UriHandler ' + uris[0]);
                showIncomingCall(uris[0]);
            }

        }

    }

    Connections {
        target: Linphone

        onReadStatus: {
            //Read stdout
            statusTextReceived = Linphone.readStatusOutput()
            //console.log("onReadStatus: " + statusTextReceived.trim())

            //No calls
            if (statusTextReceived.indexOf("No active call") !== -1) { //No need for a « && incomingCall» as we only get this when in a call we ask for «generic calls»
                console.log("onReadStatus: No acive calls")
                incomingCall = false
                //This should be done in IncomingCall.qml
                //if (incomingCallComponent.visible) PopupUtils.close(incomingCallComponent);
                onCallFav = false
                mainCol.visible = true
                answered = false
                onCall = false

                //Finish current call, so enable the speaker
                Linphone.enableSpeaker();
                speakerEnabled = true;
            }

            //Registered?
            if (statusTextReceived.indexOf("registered,") !== -1) {
                console.log("Account: Registered!")
                activeAccount.account = statusTextReceived.slice(statusTextReceived.indexOf(":") + 1, statusTextReceived.indexOf(" duration"))

                //We should check that we are not in another stdout result

            } else if (statusTextReceived.indexOf("registered=") !== -1) {
                activeAccount.account = i18n.tr("offline")
            }

            //Check if we are reciving an incoming call but we are not already in one
            if (statusTextReceived.indexOf("IncomingReceived") !== -1 && !incomingCall) {
                console.log("Register: IncomingReceived")
                incomingCall = true
                var caller = statusTextReceived.slice(statusTextReceived.indexOf("sip:") + 4)
                caller = caller.slice(0,caller.indexOf(" "))
                showIncomingCall(caller)
            } else console.log("Register: not IncomingReceived")

            if (statusTextReceived.indexOf("OutgoingRinging") !== -1 && !incomingCall) {
            }

            //We are in a call
            if (statusTextReceived.indexOf("StreamsRunning") !== -1 && !incomingCall) {
                answered = true
            }
        }
    }

    Component {
        id: incomingCallComponent

        IncomingCall {
            anchors.fill: parent
        }

    }

    function showIncomingCall(callerId) {

        //ToDo: Change :5060 by the port in config
        var callerInfo = callerId.replace(/linphone:\/\/incoming\/sip:|:5060/g,"").split("@")

        showId = callerInfo[0]
        showDomain = callerInfo[1]
        console.log("ID name: " + showId)
        PopupUtils.open(incomingCallComponent);
    }

    function addAddressToFavorite(sipAdress) {
        var contactInfo = sipAdress.split("@")
        FavContactsDB.storeContact(Date(), contactInfo[0], sipAdress, "icon")
    }

    Timer {
        id: checkStatus
        repeat: true

        onTriggered: {
            Linphone.status("register")
        }

    }

    //Fix: to develop. Delete when ready
    BottomEdge {
        id: bottomEdge
        visible: false
        height: units.gu(38)

        hint {
            text: i18n.tr("Dev")
            enabled: false
            visible: false
        }

        preloadContent: true

        //CommonActions are the common verbs and actions. Should change according to the interpreter used
        contentComponent: Page {
            header: SettingsHeader {title: i18n.tr("Development")}
            width: bottomEdge.width
            height: bottomEdge.height
            Column {
                anchors.fill: parent
                anchors.topMargin: units.gu(10)
                Row {
                    width: parent.width
                    spacing: units.gu(1)
                    TextField {
                        id: command
                        width: parent.width - buttonSend.width - units.gu(1)
                        placeholderText: i18n.tr("Send a command to Linphone")
                        //anchors.horizontalCenter: parent.horizontalCenter
                        inputMethodHints: Qt.ImhUrlCharactersOnly
                        onAccepted: buttonSend.clicked()
                    }

                    Button {
                        id: buttonSend
                        text: ">"

                        onClicked: {
                            Linphone.command(command.text.split(" "))
                        }

                    }

                }

            }

        }

    }

}
